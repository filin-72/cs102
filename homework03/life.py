import pygame
from pygame.locals import *
import random


class GameOfLife:
    def __init__(self, width=640, height=480, cell_size=10, speed=10):
        self.width = width
        self.height = height
        self.cell_size = cell_size

        self.screen_size = width, height # Устанавливаем размер окна

        self.screen = pygame.display.set_mode(self.screen_size) # Создание нового окна

        self.cell_width = self.width // self.cell_size # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_height = self.height // self.cell_size

        self.speed = speed # Скорость протекания игры


def draw_grid(self):
    ''' Нарисовать сетку '''
    for x in range(0, self.width, self.cell_size):
        pygame.draw.line(self.screen, pygame.Color('black'),
                         (x, 0), (x, self.height))
    for y in range(0, self.height, self.cell_size):
        pygame.draw.line(self.screen, pygame.Color('black'),
                         (0, y), (self.width, y))

def run(self): # Запуск игры
    pygame.init()
    clock = pygame.time.Clock()
    pygame.display.set_caption('Game of Life')
    self.screen.fill(pygame.Color('white')) # Создание списка клеток
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == QUIT:
                running = False
        self.draw_cell_list(self.clist)
        self.update_cell_list(self.clist)
        self.draw_grid()
        pygame.display.flip()
        clock.tick(self.speed)
    pygame.quit()


if __name__ == '__main__':
    game = GameOfLife(320, 240, 20)
    game.run()


def cell_list(self, randomize=False):
    """
    Создание списка клеток.

    Клетка считается живой, если ее значение равно 1.
    В противном случае клетка считается мертвой, то
    есть ее значение равно 0.
    Если параметр randomize = True, то создается список, где
    каждая клетка может быть равновероятно живой или мертвой.
    """
    pass
