import datetime as dt
from statistics import median
from typing import Optional
from api import get_friends
from api_models import User
import datetime

def age_predict(user_id: int) -> Optional[float]:
    """ Наивный прогноз возраста по возрасту друзей
    Возраст считается как медиана среди возраста всех друзей пользователя
    :param user_id: идентификатор пользователя
    :return: медианный возраст пользователя
    """
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    response = get_friends(user_id, 'bdate') 

    data = get_friends(user_id, 'bdate')
    dates = [i.get('bdate') for i in data['response']['items']]
    years = [dates[i] for i in range(len(dates)) if dates[i] is not None]
    full = [years[i] for i in range(len(years)) if len(years[i]) > 5]
    digits = [full[i].split('.') for i in range(len(full))]

    seconds = 60 * 60 * 24 * 365
    result = 0
    now = datetime.date.today()
    for i in digits:
        bdate = datetime.date(int(i[2]), int(i[1]), int(i[0]))
        res = now - bdate
        result += res.total_seconds() // seconds
    avage = result // len(digits)
    return avage

if __name__ == '__main__':
        predicted_age = age_predict(209077977) 
        print(predicted_age)
