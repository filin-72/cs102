import random


def is_prime(n: int) -> bool:
    """
    >>> is_prime(2)
    True
    >>> is_prime(11)
    True
    >>> is_prime(8)
    False
    """
    primeN = True

    for a in range(2, n):
        if (n % a == 0):
            primeN = False

    return primeN


def gcd(a: int, b: int) -> int:
    """
    >>> gcd(12, 15)
    3
    >>> gcd(3, 7)
    1
    """

    if (a % b == 0):
        return b

    return gcd(b, a % b)


def multiplicative_inverse(e: int, phi: int) -> int:
    """
    >>> multiplicative_inverse(7, 40)
    23
    """

    def gcd_2(e: int, phi: int) -> tuple:
        if phi == 0:
            return e, 1, 0
        else:
            d, x, y = gcd_2(phi, e % phi)
            return d, y, x - y * (e // phi)

    d = gcd_2(e, phi)
    res = d[1] % phi
    res_int = int(res)
    return res_int


def generate_keypair(p: int, q: int) -> tuple:
    if not (is_prime(p) and is_prime(q)):
        raise ValueError('Both numbers must be prime.')
    elif p == q:
        raise ValueError('p and q cannot be equal')

    n = p * q

    phi = (p - 1) * (q - 1)

    # Choose an integer e such that e and phi(n) are coprime
    e = random.randrange(1, phi)

    # Use Euclid's Algorithm to verify that e and phi(n) are comprime
    g = gcd(e, phi)
    while g != 1:
        e = random.randrange(1, phi)
        g = gcd(e, phi)

    # Use Extended Euclid's Algorithm to generate the private key
    d = multiplicative_inverse(e, phi)
    # Return public and private keypair
    # Public key is (e, n) and private key is (d, n)
    return ((e, n), (d, n))


def encrypt(pk: tuple, plaintext: str) -> str:
    # Unpack the key into it's components
    key, n = pk
    # Convert each letter in the plaintext to numbers based on
    # the character using a^b mod m
    cipher = [ord(char ** key) % n for char in plaintext]
    # Return the array of bytes
    cipher1 = str(cipher)
    return cipher1


def decrypt(pk: tuple, ciphertext: str) -> str:
    # Unpack the key into its components
    key, n = pk
    # Generate the plaintext based on the ciphertext and key using a^b mod m
    plain = [chr((char ** key) % n) for char in ciphertext]
    # Return the array of bytes as a string
    return ''.join(plain)


if __name__ == '__main__':
    print("RSA Encrypter/ Decrypter")

    primeN = 0
    while primeN < 1:
        p = int(input("Enter a prime number: "))
        if (is_prime(p)):
            primeN += 1
        else:
            print("NOT A PRIME NUMBER!")

    primeN = 0
    while primeN < 1:
        q = int(input("Enter another prime number: "))
        if not (is_prime(q)):
            print("NOT A PRIME NUMBER!")
        elif (q == p):
            print("MUST BE DIFFERENT!")
        elif (is_prime(q) & (q != p)):
            primeN += 1

    print("Generating your public/private keypairs now . . .")
    public, private = generate_keypair(p, q)
    print("Your public key is ", public, " and your private key is ", private)
    message = input("Enter a message to encrypt with your private key: ")
    encrypted_msg = encrypt(private, message)
    print("Your encrypted message is: ")
    print(encrypted_msg)
    print("Decrypting message with public key ", public, " . . .")
    print("Your message is:")
    print(decrypt(public, encrypted_msg))