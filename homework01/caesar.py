"""
encrypt_caesar()
"""
def encrypt_caesar(plaintext):
    """
    Encrypts plaintext using a Caesar cipher.
    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("Python3.6")
    'Sbwkrq3.6'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ''
    for text in plaintext:
        if 'a' <= text <= 'w' or 'A' <= text <= 'W':
            write = ord(ch) + 3
        if 'x' <= text <= 'z':
            write -= 26
            ciphertext += chr(write)
        else:
            ciphertext += text
    return ciphertext

"""
decrypt_caesar()
"""
def decrypt_caesar(ciphertext):
    """
    Decrypts a ciphertext using a Caesar cipher.
    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("Sbwkrq3.6")
    'Python3.6'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ''
    for text in ciphertext:
        if 'd' <= text <= 'z' or 'D' <= text <= 'z':
            write = ord(text) - 3
        if 'a' <= text <= 'c':
                write += 26
            plaintext += chr(write)
        else:
            plaintext += text
    return plaintext

